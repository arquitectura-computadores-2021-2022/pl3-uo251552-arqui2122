// Type name abbreviations
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Javier;
    PPerson pJavier;

    // Memory location of variable Javier is assigned to the pointer
    pJavier = &Javier;
    Javier.heightcm = 180;
    Javier.weightkg = 84.0;
    pJavier->weightkg = 83.2;
    return 0;
}
